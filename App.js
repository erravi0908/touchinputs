import * as React from 'react';
import { Button, View,Text } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import CustomDraw from './CustomDraw'

function Home({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Users')}
        title="Go to Users"
      />
      <Button
        onPress={() => navigation.navigate('Home_settings')}
        title="Go to Home Setting"
      />
      <Button
        onPress={() => navigation.navigate('Home_Posts')}
        title="Go to Posts"
      />
    </View>
  );
}

function Users({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.navigate('Posts')} title="Go To Posts" />
    </View>
  );
}

function Posts({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.navigate('Home')} title="Go To Home " />
    </View>
  );
}

const Drawer = createDrawerNavigator();
const Stack=createStackNavigator();


const HomeStack=() =>
{
return(
      <Stack.Navigator>
      <Stack.Screen name="Home" component={Home}/>   
     
      <Stack.Screen name="Home_settings" >
        {props=><Text> Home Setting </Text>}
      </Stack.Screen>
     
      <Stack.Screen name="Home_Posts" >
        {props=><Text> Home Posts </Text>}
      </Stack.Screen>
     </Stack.Navigator>
 
)}


//HOME Main Route
//  .settings' ((Stack NAvigation))
//  .Users
//USERS (Tab NAvigation)
//  .settings'
//  .Users
//  POSTS (Simple Route)

//MAin Navigator : DRAWER based

const Tab=createBottomTabNavigator();

const UserTab=()=>(
<Tab.Navigator>
<Tab.Screen name="Users" component={Users} />
<Tab.Screen name="Users_Setting">
  {props=><Text>Users_Setting</Text>}
</Tab.Screen>
<Tab.Screen name="Users_Profiles">
  {props=><Text>Users_Profiles</Text>}
</Tab.Screen>
</Tab.Navigator>
)




export default function App() {
  return (
   <NavigationContainer>
      <Drawer.Navigator>
      <Drawer.Screen name="Home" component={HomeStack}/>
      <Drawer.Screen name="Users" component={UserTab}/>     
      <Drawer.Screen name="Posts" >
        {props=><Text> Posts Main </Text>}
      </Drawer.Screen>
     </Drawer.Navigator>
    </NavigationContainer>
  );
}
