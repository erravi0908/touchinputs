export function setNameAction(name)
{
    return {
        type:"SET_NAME",
        payload:name
    }
}


export function setAgeAction(age)
{
    return {
        type:"SET_AGE",
        payload:age
    }
}



export function setGenderAction(gender)
{
    return {
        type:"SET_GENDER",
        payload:gender
    }
}

