export function add(number)
{
    return {
        type:"ADD",
        payload:number
    }
}

export function sub(number)
{
    return {
        type:"SUB",
        payload:number
    }
}

export function mul(number)
{
    return {
        type:"MUL",
        payload:number
    }
}