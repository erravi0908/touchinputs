import './App.css';
import User from '../components/User';
import React from "react";
import {connect } from 'react-redux';
import {setNameAction} from '../actions/userActions';
import {setAgeAction} from '../actions/userActions';

// App is a smart Component becuase it is connect to Redux now using connect(mapStateToProps,mapDispatchToProps)(App)  method
class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello, </h1>
        {/* // passing username to UserComponent, which is a dumb component i.e stateless component
            // this UserComponent gets its data from props; so it is a dumb component or called PResentation Components
            // They are not connected to Redux state and actions so thats why they are called Dumb components
        */}
        <User name={this.props.user.name}/>

        {/* // displaying the default username from the UserReducer  */} 
        <p> Displaying the Default Name Passed From index.js 'userReducer' to the App Component ----  {this.props.user.name}</p>
        
        {/* // displaying the default Age from the UserReducer  */} 
        <p> Displaying the Default Age Passed From index.js 'userReducer' to the App Component ----  {this.props.user.age}</p>
        
        {/* // calling the App component local method setName( ) and using it passing the action and dispatching it to the  UserReducer  */} 
        <button onClick={()=>this.props.setName("Anna")}>Click to change Default Name  </button>
        
        {/* // calling the App component local method setAge( ) and using it passing the action and dispatching it to the  UserReducer  */} 
        <button onClick={()=>this.props.setAge("22")}>Click to change Default Age  </button>
      </div>
    );



  }
}


const mapStateToProps = (state) => {
  return {
    //component localProperties : Global State stateProperties
    user: state.userReducer,
    math: state.mathReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // component localMethods : Global state actions Dispatch 
    setName: (name) => { 
          dispatch( setNameAction(name))
         },

         // component localMethods : Global state actions Dispatch 
         setAge: (age) => { 
          dispatch( setAgeAction(age))
         },
  }
}



//connect(mapStateToProps,mapDispatchToProps (App));
//export default App;
export default connect(mapStateToProps,mapDispatchToProps)(App);
