




//initial state
const initialState = {
    result: 1000,
    lastValues:[]
  }


//  reducer takes action and state as input
// and returns state i.e updated

const mathReducer = (state=initialState,action) => {
    //state

    //console.log(Object.getOwnPropertyNames(action));
    console.log(Object.getOwnPropertyNames(state));
    
  switch (action.type) {
    case "ADD":
      {

        console.log("ADD Block called");
        
        state = {
          ...state,
          result : state.result+action.payload,
          lastValues : [...state.lastValues,action.payload]
         
        }
        console.log("ADD " + JSON.stringify(state));
        
        break;
      }

    case "SUB":
      {
        
        console.log("SUB Block called");
        state={
          ...state,
          result:state.result-action.payload,
          lastValues : [...state.lastValues,action.payload]
        
        }
        console.log("SUB " + JSON.stringify(state));
        break;
      }

    case "MUL":
      {

        console.log("MUL Block called");
        state={
          ...state,
          result:state.result*action.payload,
          lastValues : [...state.lastValues,action.payload]
        
        }

        console.log("MUL " + JSON.stringify(state));
        break;
      }
    default:
      {
        console.log("Default " + JSON.stringify(state));
        return state;
      }

  }




  return state;
}

export default mathReducer;