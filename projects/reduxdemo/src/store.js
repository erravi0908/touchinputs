
import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';

import mathReducer from './reducers/mathReducer';
import userReducer from './reducers/userReducer';


// my logger
const myLogger=(storee)=>(next)=>(action)=>{
    console.log("Logged ACtion" + action)
    next(action)
    }
    


//to create store,
// using createStore(reducers,state) or
// createStore(reducers) or
//  createStore(combineReducers({ mathReducer, userReducer }), {optional state object},applyMiddleware(myLogger, logger))

export default createStore(combineReducers({ mathReducer, userReducer }), {},
    applyMiddleware(myLogger, logger));




// call back that is executed whenever the store gets updated
// store.subscribe(() => {
//   console.log("---------Subscribe CallBack---------------" );
//   console.log("Subscribe Callback store called ----->  " + store.getState() );
//   console.log("---------Subscribe CallBack Finished---------------" );
// })