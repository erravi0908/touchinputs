import React, { useState } from 'react';
import { View, Text } from 'react-native';//
import { StyleSheet } from 'react-native';
import { TextInput } from 'react-native';
import { Button } from 'react-native';



const TextInputComponent = () => {

    const [content, setContent] = useState('');
    const [userNames,setUserNames]=useState([]);



    // method that is executed on each key pressed
    // it stores the text into content variable
    const inputEntered = (e) => {      
        setContent(e)
    }

    // method that is executed when submit is clicked to show what is inserted till now
    const submitPressed = () => {
         const userList=[...userNames,content] ;
        setUserNames(userList)
        setContent('')
       }

       //to generate unique no fo keys
       const genKey=() => {
        
        return(Math.random(100*1)+10)
      }


    return (

        <View style={styles.container}>

            <View style={styles.childViewStyle} >

                <Text style={styles.textStyle}>Enter your User Names below </Text>
                <TextInput
                    style={styles.textInputStyle}
                    onChangeText={(e) => { inputEntered(e) }}
                >
                </TextInput>

            </View>

            <View style={styles.paddingStyle}>
                <Button onPress={() => submitPressed()}
                    title="Click to Add User to the list"
                    style={styles.buttonStyle} />
            </View>

            <View tyle={styles.listStyle}>

                {userNames ?
                userNames.map(userName=>
                (<Text s key={userName+genKey() }>{userName}</Text>))
                 : null}

                   
                

            </View>



        </View>


    )


}

const styles = StyleSheet.create({
    container: {

        justifyContent: "flex-start",
        alignItems: "flex-start",
        backgroundColor: "grey",
        borderWidth: 2,
        padding: 20,
        margin: 10,
        width: "100%",
        height: "auto"
    },

    childViewStyle:
    {
        backgroundColor: "grey",
        color: 'black',
        fontSize: 20,
        borderWidth: 2,
        height: "auto",
        width: "100%"


    },

    textInputStyle:
    {
        backgroundColor: "white",
        borderWidth: 1,
        height: "auto"


    },
    textStyle:
    {
        backgroundColor: "grey",
        color: 'black',
        fontSize: 20,
        borderWidth: .5,
        height: "auto",
        width: "100%"


    },
    paddingStyle:
    {
        padding: 5,
        width: "100%"
    },
    buttonStyle:
    {
        alignItems: "center",
        alignContent: "center",
    },

    listStyle:
    {
       
        color: 'red',
        height: "auto",
        width: "100%"

    }



});

export default TextInputComponent;
