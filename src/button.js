import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const ButtonComponent = () => {

  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  }

  const resetCount = () => {
    setCount(0);
  }

  return (
    <View style={styles.container}>


      <View>
        <Text>{count?count:null}</Text>

      </View>


      <Button title="Click to increase count" onPress={increment} />
      
      <View style={{padding:10}}>
        
      </View>


      <Button title="Reset Count" onPress={resetCount} />
    </View>


  )


}




//creating the style Component

const styles = StyleSheet.create({
  container: {
   
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "red",
    padding: 20,
    margin: 10,
    width:"100%",
    height:"auto"
  }
});

export default ButtonComponent;